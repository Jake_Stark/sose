(function (){
  'use strict';

  var util = require('util');

  var domain = "";
  process.argv.forEach(function (val, index, array) {
    var arg = val.split("=");
    if (arg.length > 1) {
      if (arg[0] == "--domain") {
        domain = "." + arg[1];
        console.log("Setting domain to:", domain);
      }
    }
  });
  var haproxy_domain = "127.0.0.1:5000"; 
  module.exports = {
    catalogueUrl:  util.format("http://%s%s", haproxy_domain, domain),           // catalogue%s
    tagsUrl:       util.format("http://%s%s/tags", haproxy_domain, domain),      // catalogue%s/tags
    cartsUrl:      util.format("http://%s%s/carts", haproxy_domain, domain),     // carts%s/carts
    ordersUrl:     util.format("http://%s%s", haproxy_domain, domain),           // orders%s 
    customersUrl:  util.format("http://%s%s/customers", haproxy_domain, domain), // user%s/customers
    addressUrl:    util.format("http://%s%s/addresses", haproxy_domain, domain), // user%s/addresses
    cardsUrl:      util.format("http://%s%s/cards", haproxy_domain, domain),     // user%s/cards
    loginUrl:      util.format("http://%s%s/login", haproxy_domain, domain),     // user%s/login
    registerUrl:   util.format("http://%s%s/register", haproxy_domain, domain),  // user%s/register
  };
}());
