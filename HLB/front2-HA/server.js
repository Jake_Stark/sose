var request      = require("request")
  , express      = require("express")
  , morgan       = require("morgan")
  , path         = require("path")
  , bodyParser   = require("body-parser")
  , async        = require("async")
  , cookieParser = require("cookie-parser")
  , session      = require("express-session")
  , config       = require("./config")
  , helpers      = require("./helpers")
  , cart         = require("./api/cart")
  , catalogue    = require("./api/catalogue")
  , orders       = require("./api/orders")
  , user         = require("./api/user")
  , metrics      = require("./api/metrics")
  , app          = express()
  , fs           = require('fs') //this engine requires the fs module
  , os           = require('os') // os info
  , consul 		 = require('consul')({host:'consul'}) // consul agent
  , util		 = require('util')

app.use(helpers.rewriteSlash);
app.use(metrics);
app.use(express.static("public"));

app.engine('me', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(new Error(err));
    // this is an extremely simple template engine
    var rendered = content.toString().replace('#title#', ''+ options.title +'')
    .replace('#message#', ''+ options.message +'');
    return callback(null, rendered);
  });
});
app.set('views', './public'); // specify the views directory
app.set('view engine', 'me'); // register the template engine


//app.set('views', './views')
//app.set('view engine', 'pug');
app.get('/', function (req, res) {
  var info = os.networkInterfaces().eth0[0];
  var addr = info.address;
  var mac = info.mac;
  res.render('index', { title: addr, message: mac});
  //res.send('hello!');
});


if(process.env.SESSION_REDIS) {
    console.log('Using the redis based session manager');
    app.use(session(config.session_redis));
}
else {
    console.log('Using local session manager');
    app.use(session(config.session));
}

app.use(bodyParser.json());
app.use(cookieParser());
app.use(helpers.sessionMiddleware);
app.use(morgan("dev", {}));

var domain = "";
process.argv.forEach(function (val, index, array) {
  var arg = val.split("=");
  if (arg.length > 1) {
    if (arg[0] == "--domain") {
      domain = arg[1];
      console.log("Setting domain to:", domain);
    }
  }
});

/* Mount API endpoints */
app.use(cart);
app.use(catalogue);
app.use(orders);
app.use(user);

app.use(helpers.errorHandler);


// HEALTH CHECK ENDPOINT
app.get('/health', (req, res) => {
	res.status(200);
	res.send('ok');
	res.end();
});




var server = app.listen(process.env.PORT || 8079, function () {
  var port = server.address().port;
  console.log("App now running in %s mode on port %d", app.get("env"), port);
  var info = os.networkInterfaces().eth0[0];
  
  
  	//SERVICE AND HEALTH CHECK REGISTRATION
	consul.agent.service.register({
	    id: util.format('%s-%s', 'front2', info.address),
		name:'front2',
		address: info.address,
		port: Number(process.env.PORT),
		check:{
			http: util.format('http://%s:%s/health', info.address, process.env.PORT ),//'http://${process.env.HOST}:${process.env.PORT}/health',
			interval:'10s'
		}
	}, err =>{if (err) throw err});
  
});
