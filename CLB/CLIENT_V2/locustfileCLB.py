# CLB - non server HAProxy in locale perché invaliderebbe i test( diventarebbe uguale a SLB)
# auto-discovery delle istance servizi di front di tipo 1, 2 e 3 

import base64
import json
import consul

from locust import HttpLocust, TaskSet, task, between
from random import randint, choice


class WebTasks(TaskSet):

    def on_start(self):
      # on_start: Viene eseguito solo all'avvio
      #
      # Effettuo auto-discovery degli ip per ogni enpoints front1,2 e 3
      # recuperando ip e porta
      
      results = [] 
      service = consul.Client(endpoint='http://consul:8500')

      front1s = service.info(name='front1')
      for front in  front1s:
        results.append(front['ServiceAddress'] +":"+ str(front['ServicePort']))
      
      front2s = service.info(name='front2')
      for front in  front2s:
        results.append(front['ServiceAddress'] +":"+ str(front['ServicePort']))
      
      front3s = service.info(name='front3')
      for front in  front3s:
        results.append(front['ServiceAddress'] +":"+ str(front['ServicePort']))
    

      # N.B. per non perdere tempo al momento della scelta tra 30 host diversi 
      # e per rendere quindi i file locust piu simili tra loro tra le diverse configurazioni 
      # imposto momentaneamente i tre endpoint  front1, front2, front3, in maniera statica
      # dopodichè penserà la rete interna di docker a restituire una istanza, tra le n.
      
      #self.endpoints = results
      self.endpoints = ["front1:8079","front2:8079","front3:8079"]

      #per la login 
      data = "user:password"
      base64Bytes  = base64.b64encode(data.encode("utf-8"))
      self.base64string = str(base64Bytes, "utf-8")

    @task
    def load(self):

        # Per non invalidare i test, dobbiamo fare in modo che i locustfile siano
        # molto simili tra loro nelle tre varie configurazioni (HLB,CLB;SLB)
        # 
        # Importante: Ciclare in questo modo renderebbe i test diversi
        # oltre che ad ogni passo verrebbero fatte n-query una per ogni enpoints
        # for endpoint in self.endpoints:
        #   print("ENDPOINT:" + endpoint)
        
        # Invece ad ogni passo facciamo una scelta randomica tra i possibili endpoints
        endpoint = choice(self.endpoints)

        host = "http://" + endpoint 
        hostname = "front_" + endpoint # ip:port

        response = self.client.get(host + "/catalogue", name=hostname+"/catalogue")
        catalogueJson = json.loads(response.content)
        category_item = choice(catalogueJson)
        item_id = category_item["id"]
        #print(item_id)
        self.client.get(host + "/", name=hostname+"/")
        self.client.get(host + "/login", headers={"Authorization":"Basic %s" % self.base64string}, name=hostname+"/login")
        self.client.get(host + "/category.html", name=hostname+"/category.html")
        self.client.get(host + "/detail.html?id={}".format(item_id), name=hostname+"/detail.html?id={}".format(item_id))
        self.client.delete(host + "/cart", name=hostname+"/cart")
        self.client.post(host + "/cart", json={"id": item_id, "quantity": 1}, name=hostname+"/cart")
        self.client.get(host + "/basket.html", name=hostname+"/basket.html")
        self.client.post(host + "/orders", name=hostname+"/orders")


class Web(HttpLocust):
    host ="http://localhost:5000" # fake, necessario per locust,  viene sovrascritto nel task
    task_set = WebTasks
    wait_time = between(0, 0) # wait 0
