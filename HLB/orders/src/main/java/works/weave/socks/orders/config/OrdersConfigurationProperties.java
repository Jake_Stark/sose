package works.weave.socks.orders.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;

@ConfigurationProperties
public class OrdersConfigurationProperties {

	private String hostname = "localhost";
    private String domain = "";
    private String port = "5000"; // haproxy


    public URI getPaymentUri() {
        //return new ServiceUri(new Hostname("payment"), new Domain(domain), "/paymentAuth").toUri();
        return new ServiceUri(new Hostname("localhost"), new Domain(domain), new Port("5000"),  "/paymentAuth").toUri();

    }

    public URI getShippingUri() {
        //return new ServiceUri(new Hostname("shipping"), new Domain(domain), "/shipping").toUri();
        return new ServiceUri(new Hostname("localhost"), new Domain(domain), new Port("5000"), "/shipping").toUri();

    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    public void setPort(String port) { this.port = port; }

    private class Hostname {
        private final String hostname;

        private Hostname(String hostname) {
            this.hostname = hostname;
        }

        @Override
        public String toString() {
            if (hostname != null && !hostname.equals("")) {
                return hostname;
            } else {
                return "";
            }
        }
    }

    private class Domain {
        private final String domain;

        private Domain(String domain) {
            this.domain = domain;
        }

        @Override
        public String toString() {
            if (domain != null && !domain.equals("")) {
                return "." + domain;
            } else {
                return "";
            }
        }
    }

    private class Port {
        private final String port;

        private Port(String port) {
            this.port = port;
        }

        @Override
        public String toString() {
            if (port != null && !port.equals("")) {
                return ":" + port;
            } else {
                return "";
            }
        }
    }

    private class ServiceUri {
        private final Hostname hostname;
        private final Domain domain;
        private final Port port;
        private final String endpoint;

        private ServiceUri(Hostname hostname, Domain domain, Port port, String endpoint) {
            this.hostname = hostname;
            this.domain = domain;
            this.port = port;
            this.endpoint = endpoint;
        }

        public URI toUri() {
            return URI.create(wrapHTTP(hostname.toString() + domain.toString(), port.toString()) + endpoint);
        }

        private String wrapHTTP(String host, String port) {
            return "http://" + host + port;
        }

        @Override
        public String toString() {
            return "ServiceUri{" +
                    "hostname=" + hostname +
                    ", domain=" + domain +
                    ", port=" + port +
                    '}';
        }
    }
}
