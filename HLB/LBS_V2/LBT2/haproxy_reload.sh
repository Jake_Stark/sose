#!/bin/sh

#[ -n "${DEBUG}" ] && set -x

# first start
#if [ ! -f /haproxy.conf.previous ]; then
#  echo "$0: First configuration"
#  cp /haproxy.conf /haproxy.conf.previous
#  haproxy -f /haproxy.conf
#  exit 0
#fi

# configuration update occured
#CHECK=$(diff -u -p /haproxy.conf.previous /haproxy.conf | egrep -c "^[-+]backend ")

# we trigger a reload only when backends have been removed or added
#if [ ${CHECK} -gt 0 ]; then
#  if [ -S /haproxy.sock ]; then
#    echo "show servers state" | socat /haproxy.sock - > /haproxy.serverstates
#  fi
#  echo "$0: Backend(s) has(ve) been added or removed, need to reload the configuration"
#  haproxy -f /haproxy.conf -sf $(cat /haproxy.pid)
#fi

#cp /haproxy.conf /haproxy.conf.previous




# get haproxy PID
HPID=$(ps -A -o pid,comm | grep haproxy | grep -v grep | head -n 1 | awk '{print $1}')


if [ -z "$HPID" ]; then
  #se vuota primo avvio
  haproxy -f /etc/haproxy/haproxy.cfg -d
  exit 0
fi
# altrimenti reload   
haproxy -f /etc/haproxy/haproxy.cfg -sf ${HPID}

# wait 5 sec
#sleep 5


#bugfix

# se lo script viene chiamato più volte, velocemente , haproxy viene avviato diverse istanze
# allora ultimo script  pulisce elimindando le vecchie istanze

# prendo ultimo pid da file
hpd=$( cat /var/run/haproxy.pid )
#echo $hpd
if [ -z "$hpd" ]; then
 #stringa vuota, non esiste
 exit 0
fi
# prendo tutte le istanze in esecuzione, elimino i pid haproxy con pid minore a hpd
for elem in ` ps -A -o pid,comm | grep haproxy | grep -v grep | awk '{print $1}') `
do
  #echo $elem elem
  if [[ $elem -lt  $hpd ]]; then
      #echo "minore kill " $elem
    kill $elem
  fi
done

#hpd=$( cat /var/run/haproxy.pid )
#echo $hpd
#while read -r pids;
#do
# echo ${pids};
#done



