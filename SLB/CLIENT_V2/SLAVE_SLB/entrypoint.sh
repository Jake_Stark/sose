#!/bin/sh
# Run Locust in Slave mode, master is clientV2
#locust -f /locust/locustfileSLB.py --$LOCUST_MODE --master-host $LOCUST_MASTER_HOST
locust -f $LOCUSTFILE_PATH --$LOCUST_MODE --master-host $LOCUST_MASTER_HOST
