#!/usr/bin/env bash

PORT="80"
IP=`ip addr | grep -E 'eth0.*state UP' -A2 | tail -n 1 | awk '{print $2}' | cut -f1 -d '/'`
NAME="lb1"
read -r -d '' MSG << EOM
{
  "Name": "$NAME",
  "address": "$IP",
  "port": $PORT,
  "Check": {
     "http": "http://$IP:$PORT",
     "interval": "10s"
  }
}
EOM
echo $MSG
curl -v -XPUT -d "$MSG" http://consul:8500/v1/agent/service/register


