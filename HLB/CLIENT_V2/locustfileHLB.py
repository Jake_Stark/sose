# HLB - non server HAProxy in locale perché invaliderebbe i test( diventarebbe uguale a SLB)
# auto-discovery delle istance servizi di lbt di tipo 1, 2 e 3 

import base64
import json
import consul

from locust import HttpLocust, TaskSet, task, between
from random import randint, choice


class WebTasks(TaskSet):

    def on_start(self):
      # on_start: Viene eseguido solo all'avvio
      # Gli enpoints potrebbero essere messi staticamente dentro results, in quanto sono
      # 3 lb ben distinti e definiti (http://lbt1:80, http://lbt2:80, http://lbt3:80 )
      # ma per completezza e dinamicità della porta, anche futura 
      # effettuo un auto-discovery degli ip per ognuno di essi
      # recuperando ip e porta

      results = [] # ["http://lbt1:80","http://lbt2:80","http://lbt3:80"]
      service = consul.Client(endpoint='http://consul:8500')

      lbt1s = service.info(name='lbt1')
      for lbt in  lbt1s:
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))
      
      lbt2s = service.info(name='lbt2')
      for lbt in  lbt2s:
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))
      
      lbt3s = service.info(name='lbt3')
      for lbt in  lbt3s:
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))

      self.endpoints = results
      
      # per la login 
      data = "user:password"
      base64Bytes  = base64.b64encode(data.encode("utf-8"))
      self.base64string = str(base64Bytes, "utf-8")


    @task
    def load(self):

        # Per non invalidare i test, dobbiamo fare in modo che i locustfile siano
        # molto simili tra loro nelle tre varie configurazioni (HLB,CLB;SLB)
        # 
        # Importante: Ciclare in questo modo renderebbe i test diversi
        # oltre che ad ogni passo verrebbero fatte n-query una per ogni enpoints
        # for endpoint in self.endpoints:
        #   print("ENDPOINT:" + endpoint)
        
        # Invece ad ogni passo facciamo una scelta randomica tra i possibili endpoints
        endpoint = choice(self.endpoints)

        host = "http://" + endpoint 
        hostname = "lbt_" + endpoint # ip:port

        response = self.client.get(host + "/catalogue", name=hostname+"/catalogue")
        catalogueJson = json.loads(response.content)
        category_item = choice(catalogueJson)
        item_id = category_item["id"]
        #print(item_id)
        self.client.get(host + "/", name=hostname+"/")
        self.client.get(host + "/login", headers={"Authorization":"Basic %s" % self.base64string}, name=hostname+"/login")
        self.client.get(host + "/category.html", name=hostname+"/category.html")
        self.client.get(host + "/detail.html?id={}".format(item_id), name=hostname+"/detail.html?id={}".format(item_id))
        self.client.delete(host + "/cart", name=hostname+"/cart")
        self.client.post(host + "/cart", json={"id": item_id, "quantity": 1}, name=hostname+"/cart")
        self.client.get(host + "/basket.html", name=hostname+"/basket.html")
        self.client.post(host + "/orders", name=hostname+"/orders")


class Web(HttpLocust):
    host ="http://localhost:5000" # fake, necessario per locust,  viene sovrascritto nel task
    task_set = WebTasks
    wait_time = between(0, 0) # wait 0
