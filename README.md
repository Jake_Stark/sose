# SOSE- HYBRID LB #
### versione2 :
- Raccolta metriche e monitoring Grafana - Prometheus - cAdvisor
- Template haproxy dinamico con consult-client
- Locust.io in configurazione distribuita  master/slave

Il codice per effettuare le build dei vari container è suddiviso in 3 cartelle differenti, rispettivamente per le 3 diverse configurazioni (o approcci ), ognuna contentente un docker-compose.yaml file, al suo interno, da eseguire separatamente .

### Tipi di Configurazioni###

* CLB = Client Side Load Balancing
* SLB = Server Side Load Balancing
* HLB = Hybrid Side Load Balancing

#### Nota:
Utilizzare i file versione 2, (docker-composeV2.yml) quindi indicare a compose tale file
in fase di avvio con l'opzione -f :

$ docker-compose -f docker-composeV2.yml up -d ( up -d , avvio di tutti i servizi in background )

Inoltre per modifiche future fare riferimento alla versione "2.x" di compose file :
https://docs.docker.com/compose/compose-file/compose-file-v2/

#### Nota (2):
Nelle 3 configurazioni sono state definite tramite il flag "scale",
6 istanze totali per configurazione/tipo per il/i servizio/i "front".

Inoltre i nodi slave di partenza sono già  impostati a 10.

Per problematiche legate al nodo consul, la max configurazione funzionante è pari a :

* 15 istanze totali di front e 10  nodi slave.
* ovvero per SLB front1=15, per CLB e HLB front1=5, front2=5, front3=5

Per scalare le istanze di un servizio eseguiamo il comando

* $ docker-compose -f <compose-file.yml> scale <nome-servizio> = <#-istanze>

esempio (in conf. SLB):

* $docker-compose -f docker-composeV2.yml scale front1= 15   (in conf. SLB)




