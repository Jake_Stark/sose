#!/bin/sh
# Run HaProxy and Registration to Consul
sh /consul/registration.sh
haproxy -f /etc/haproxy/haproxy.cfg -d &
consul-template -consul-addr=http://consul:8500 -template "/etc/haproxy/haproxy.tlp:/etc/haproxy/haproxy.cfg:haproxy_reload.sh" -log-level=debug
#haproxy -f /etc/haproxy/haproxy.cfg -d
