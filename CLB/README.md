# Configurazione CLB #


### Build per Microservizi in JAVA ###
#### N.B. : Eliminare la cartella target (se esiste) prima di eseguire la Build

* Orders -  dentro la cartella => :~../orders$  GROUP=dev COMMIT=CLB_latest ./scripts/build.sh

### Build per CLIENT ###

* Client (master)=>  :~ ../CLIENT_V2$ docker build -t dev/client_v2:CLB_latest  .
* Client (slave)=>  :~ ../CLIENT_V2/SLAVE_CLB$ docker build -t dev/slave:CLB_latest  .


### vedere Readme di HLB per le altre immagini riutilizzate nel docker-compose.yml di CLB


## RUN ###
Posizionarsi nella cartella principale dove si trova il docker-composeV2.yml

Nota : Per evitare problematiche di rete avviare i microservizi uno alla volta, aspettando che diventano up & running
non appena si registrano sul nodo Consul (Service Registry):
Per cui aprire un terminale ed avviare prima Consul (senza la modalità -d --detach):

*  :~ ../CLB$ docker-compose up consul
#### N.B. per versione2 utilizzare opzione -f docker-composeV2.yml nel comando compose.

Successivamente  in un altro terminale avviare i microservizi rimanenti , avendo cura di attendere la registrazione su Consul. I servizi in GoLang sono più leggeri (15 secondi circa ) rispetto  a quelli in JAVA (1m e 30 sec circa)
quelli in GOLANG sono più leggeri. (tempistiche su un quadcore)

*  :~ ../CLB$ docker-compose up -d user
*  :~ ../CLB$ docker-compose up -d catalogue
*  :~ ../CLB$ docker-compose up -d payment

*  :~ ../CLB$ docker-compose up -d carts
*  :~ ../CLB$ docker-compose up -d shipping
*  :~ ../CLB$ docker-compose up -d orders
*  :~ ../CLB$ docker-compose up -d front1
*  :~ ../CLB$ docker-compose up -d front2
*  :~ ../CLB$ docker-compose up -d front3
*  :~ ../CLB$ docker-compose up -d slave
*  :~ ../CLB$ docker-compose up -d clientV2
*  :~ ../CLB$ docker-compose up -d grafana [ avvia tutti i rimanenti container dipendenti come Prometheus ecc... ]

### GRAFANA - ( MONITOR )
Trovare  l'id del container  clientV2 tramite il comando:

*  :~ $ docker ps

ed entrare in tale container con il comando :

*  :~ $ docker exec -it <id_container_clientV2> bash

entrare nella cartella cd /grafana e laciare ./import.sh che caricherà
il datasource e la dashboard sul container grafana.

Se la dashboard non viene caricata importarla a mano dalla webUI collegandosi
in http://localhost:3000 (fuori dal container client)con credenziali  admin - foobar e nel menu a sinistra importare una
in JSON copiando il codice di

* .../CLB/CLIENT_V2/grafana/grafana-dashboard.json

### LOCUST - ( LOADTEST )
A questo (da dentro il container clientV2) lanciare locust in modalità master dalla / con il  comando:

*  :~ $ locust -f  locust/locustfileCLB.py --master

e i relativi container slave si registreranno in maniera automatica presso di esso
( vedi entrypoint.sh, nella cartella ../CLIENT_V2/SLAVE_CLB , i parametri sono passati tramite il compose file)

Lasciare il container in esecuzione ed aprire una finestra nel browser navigando http://localhost:9991.

