# Configurazione HLB #


### Build per Microservizi in JAVA ###
#### N.B. : Eliminare la cartella target (se esiste) prima di eseguire la Build (oppure quando si vuole modificare il codice e bisogna rieseguire la build )

* Orders -  dentro la cartella => :~../orders$  GROUP=dev COMMIT=latest ./scripts/build.sh
* Carts - dentro la cartella  => :~../carts$  GROUP=dev COMMIT=latest ./scripts/build.sh
* Shipping - dentro la cartella => :~../shipping$ GROUP=dev-test COMMIT=latest ./scripts/build.sh
* Queue-Master - dentro la cartella => :~../queue-master$ GROUP=dev-test COMMIT=latest ./scripts/build.sh

### Build per Microservizi in GOLANG ###
#### N.B. : I comandi seguenti contengono il "punto" alla fine (da non dimenticare)

* Payment => :~../payment$ docker build -t dev/payment:latest -f ./docker/payment/Dockerfile-original .
* User => :~../user-master$ docker build -t dev/user:latest -f ./docker/user/Dockerfile-release .
* Catalogue => :~../catalogue-master$ docker build -t dev/catalogue:consul -f ./docker/catalogue/Dockerfile-original .

### Build per Microservizi in NODEJS ###

* Front1=> :~ ../front1-HA$ docker build -t dev/front1:ha-latest  .
* Front2 => :~ ../front2-HA$ docker build -t dev/front2:ha-latest  .
* Front3 => :~ ../front3-HA$ docker build -t dev/front3:ha-latest  .

### Build per Microservizi CLIENT ###

* Client (master)=>  :~ ../CLIENT_V2$ docker build -t dev/client_v2:latest  .
* Client (slave)=>  :~ ../CLIENT_V2/SLAVE_HLB$ docker build -t dev/slave:latest  .

### Build per Microservizi LBS (Load Balancers di tipo Server) ###
### versione 1 (non utilizzare)
* LB1 (LBS davanti a FRONT1 ) =>  :~ ../LB1$ docker build -t dev/lb1:latest  .
* LB2 (LBS davanti a FRONT2 ) =>  :~ ../LB2$ docker build -t dev/lb2:latest  .
* LB3 (LBS tra ORDER e SHIPPING ) =>  :~ ../LB3$ docker build -t dev/lb3:latest  .


### versione 2 (T= template HAProxy dinamico):
#### Nota: Consultare il README presente nella cartella LBS_V2
* LBT1 (LBS davanti a FRONT1 ) =>  :~ ..LBS_V2/LBT1$ docker build -t dev/lbt1:latest  .
* LBT2 (LBS davanti a FRONT2 ) =>  :~ ..LBS_V2/LBT2$ docker build -t dev/lbt2:latest  .
* LBT3 (LBS davanti a FRONT3 ) =>  :~ ..LBS_V2/LBT3$ docker build -t dev/lbt3:latest  .
* LB3 (LBS tra ORDER e SHIPPING ) =>  :~ ../LBS/LB3$ docker build -t dev/lb3:latest  .

### Immagini usate direttamente dal docker hub
#### N.B. : se le immagini non sono presenti in locale, avviene il pulling in automatico.
* consul:                    consul
* carts-db:                 mongo:3.4
* catalogue-db:         weaveworksdemos/catalogue-db
* user-db:                  weaveworksdemos/user-db
* orders-db:               mongo:3.4
* rabbitmq (queue):   rabbitmq:3.8-management

## RUN ###
Posizionarsi nella cartella principale dove si trova il docker-compose.yml

Nota : Per evitare problematiche di rete avviare i microservizi uno alla volta, aspettando che diventano up & running
non appena si registrano sul nodo Consul (Service Registry):
Per cui aprire un terminale ed avviare prima Consul (senza la modalità -d --detach):

*  :~ ../HLB$ docker-compose up consul
#### N.B. per versione2 utilizzare opzione -f docker-composeV2.yml nel comando compose.

Successivamente  in un altro terminale avviare i microservizi rimanenti , avendo cura di attendere la registrazione su Consul. I servizi in GoLang sono più leggeri (15 secondi circa ) rispetto  a quelli in JAVA (1m e 30 sec circa)
quelli in GOLANG sono più leggeri. (tempistiche su un quadcore)

*  :~ ../HLB$ docker-compose up -d user
*  :~ ../HLB$ docker-compose up -d catalogue
*  :~ ../HLB$ docker-compose up -d payment

*  :~ ../HLB$ docker-compose up -d carts
*  :~ ../HLB$ docker-compose up -d shipping
*  :~ ../HLB$ docker-compose up -d lb3

*  :~ ../HLB$ docker-compose up -d orders
*  :~ ../HLB$ docker-compose up -d front1
*  :~ ../HLB$ docker-compose up -d front2
*  :~ ../HLB$ docker-compose up -d front3 (solo v2)

*  :~ ../HLB$ docker-compose up -d lb1 (no v2  - non utilizzare )
*  :~ ../HLB$ docker-compose up -d lb2 (no v2 - non utilizzare )
*  :~ ../HLB$ docker-compose up -d lbt1 (solo v2)
*  :~ ../HLB$ docker-compose up -d lbt2 (solo v2)
*  :~ ../HLB$ docker-compose up -d lbt3 (solo v2)
*  :~ ../HLB$ docker-compose up -d client (no v2 - non utilizzare )
*  :~ ../HLB$ docker-compose up -d slave (solo v2)
*  :~ ../HLB$ docker-compose up -d clientV2 (solo v2)
*  :~ ../HLB$ docker-compose up -d grafana (solo v2) [ avvia tutti i rimanenti container dipendenti come Prometheus ecc... ]

### GRAFANA - ( MONITOR )
Trovare  l'id del container  clientV2 tramite il comando:

*  :~ $ docker ps

ed entrare in tale container con il comando :

*  :~ $ docker exec -it <id_container_clientV2> bash

entrare nella cartella cd /grafana e laciare ./import.sh che caricherà
il datasource e la dashboard sul container grafana.

Se la dashboard non dovesse essere caricata (accade se il JSON non è corretto )
importarla a mano dalla webUI collegandosi in http://localhost:3000 (fuori dal container client)
con credenziali  admin - foobar e nel menu a sinistra importarne una in JSON
copiando il codice di:

* .../HLB/CLIENT_V2/grafana/grafana-dashboard.json

### LOCUST - ( LOADTEST )
A questo (sempre da dentro il container clientV2) lanciare locust in modalità master dalla / con il  comando:

*  :~ $  locust -f  locust/locustfileHLB.py --master

e i relativi container slave si registreranno in maniera automatica presso di esso
( vedi entrypoint.sh, nella cartella ../CLIENT_V2/SLAVE_HLB , i parametri sono passati tramite il compose file)

Lasciare il container in esecuzione ed aprire una finestra nel browser navigando http://localhost:9991.

