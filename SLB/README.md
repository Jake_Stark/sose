# Configurazione SLB #


### Build per Microservizi in JAVA ###
#### N.B. : Eliminare la cartella target (se esiste) prima di eseguire la Build

* Orders -  dentro la cartella => :~../orders$  GROUP=dev COMMIT=SLB_latest ./scripts/build.sh

### Build per Microservizi in NODEJS ###

* Front1=> :~ ../front1$ docker build -t dev/front1:SLB_latest  .


### Build per CLIENT ###

* Client (master)=>  :~ ../CLIENT_V2$ docker build -t dev/client_v2:SLB_latest  .
* Client (slave)=>  :~ ../CLIENT_V2/SLAVE_SLB$ docker build -t dev/slave:SLB_latest  .


### vedere  Readme di HLB per le altre immagini riutilizzate nel docker-compose.yml di SLB


## RUN ###
Posizionarsi nella cartella principale dove si trova il docker-composeV2.yml

Nota : Per evitare problematiche di rete avviare i microservizi uno alla volta, aspettando che diventano up & running
non appena si registrano sul nodo Consul (Service Registry):
Per cui aprire un terminale ed avviare prima Consul (senza la modalità -d --detach):

*  :~ ../SLB$ docker-compose up consul
#### N.B. per versione2 utilizzare opzione -f docker-composeV2.yml nel comando compose.

Successivamente  in un altro terminale avviare i microservizi rimanenti , avendo cura di attendere la registrazione su Consul. I servizi in GoLang sono più leggeri (15 secondi circa ) rispetto  a quelli in JAVA (1m e 30 sec circa)
quelli in GOLANG sono più leggeri. (tempistiche su un quadcore)

*  :~ ../SLB$ docker-compose up -d user
*  :~ ../SLB$ docker-compose up -d catalogue
*  :~ ../SLB$ docker-compose up -d payment

*  :~ ../SLB$ docker-compose up -d carts
*  :~ ../SLB$ docker-compose up -d shipping
*  :~ ../SLB$ docker-compose up -d lb3
*  :~ ../SLB$ docker-compose up -d orders
*  :~ ../SLB$ docker-compose up -d front1
*  :~ ../SLB$ docker-compose up -d lbt1
*  :~ ../SLB$ docker-compose up -d slave
*  :~ ../SLB$ docker-compose up -d clientV2
*  :~ ../SLB$ docker-compose up -d grafana

### GRAFANA - ( MONITOR )
Trovare  l'id del container  clientV2 tramite il comando:

*  :~ $ docker ps

ed entrare in tale container con il comando :

*  :~ $ docker exec -it <id_container_clientV2> bash

entrare nella cartella cd /grafana e laciare ./import.sh che caricherà
il datasource e la dashboard sul container grafana.

Se la dashboard non viene caricata importarla a mano dalla webUI collegandosi
in http://localhost:3000 (fuori dal container client)con credenziali  admin - foobar e nel menua sinistra importare una
in JSON copiando il codice di

* .../SLB/CLIENT_V2/grafana/grafana-dashboard.json

### LOCUST - ( LOADTEST )
A questo (da dentro il container clientV2) lanciare locust in modalità master dalla / con il  comando:

*  :~ $  locust -f  locust/locustfileSLB.py --master

e i relativi container slave si registreranno in maniera automatica presso di esso
( vedi entrypoint.sh, nella cartella ../CLIENT_V2/SLAVE_SLB , i parametri sono passati tramite il compose file)

Lasciare il container in esecuzione ed aprire una finestra nel browser navigando http://localhost:9991.

