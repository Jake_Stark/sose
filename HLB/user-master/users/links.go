package users

import (
	"flag"
	"fmt"
	"os"
)

var (
	domain    string
	entitymap = map[string]string{
		"customer": "customers",
		"address":  "addresses",
		"card":     "cards",
	}
)

func init() {
	flag.StringVar(&domain, "link-domain", os.Getenv("HATEAOS"), "HATEAOS link domain")
}

//type Links map[string]Href
type Links map[string]HrefMap
//# type HrefMap map[string]string
type HrefMap map[string]interface{}

func (l *Links) AddLink(ent string, id string) {
	fmt.Println("AddLink") //
	nl := make(Links)
	link := fmt.Sprintf("http://%v/%v/%v", domain, entitymap[ent], id)
	fmt.Println("debug: valore link->",link)
	//nl[ent] = Href{link}
	//nl["self"] = Href{link}
	/////nl["self"] = Href{"http://test.ciao"}
	//# hm := make(HrefMap)
	//hm["href"] = Href{link}
	//# hm["href"] = link
	//# nl[ent] = hm
	//# nl["self"] = hm
	m := HrefMap{"href":link, "templated":true}
	nl[ent] = m
	nl["self"] = m
	fmt.Println("debug: valore nl (prima)->",nl)
	*l = nl
	fmt.Println("debug: valore nl (dopo)->",nl)
	fmt.Println("debug: valore *l->",*l)

}

func (l *Links) AddAttrLink(attr string, corent string, id string) {
	link := fmt.Sprintf("http://%v/%v/%v/%v", domain, entitymap[corent], id, entitymap[attr])
	nl := *l
	//# hm := make(HrefMap)
	//# hm["href"] = link
	m := HrefMap{"href":link, "templated":true}

	//nl[entitymap[attr]] = Href{link}
	//# nl[entitymap[attr]] = hm
	nl[entitymap[attr]] = m
	*l = nl
}

func (l *Links) AddCustomer(id string) {
	l.AddLink("customer", id)
	l.AddAttrLink("address", "customer", id)
	l.AddAttrLink("card", "customer", id)
}

func (l *Links) AddAddress(id string) {
	l.AddLink("address", id)
}

func (l *Links) AddCard(id string) {
	l.AddLink("card", id)
}

type Href struct {
	string `json:"string"`
}

