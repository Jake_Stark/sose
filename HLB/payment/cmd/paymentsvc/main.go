package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"strconv"

	"github.com/go-kit/kit/log"
	"github.com/microservices-demo/payment"
	stdopentracing "github.com/opentracing/opentracing-go"
	zipkin "github.com/openzipkin/zipkin-go-opentracing"
	"golang.org/x/net/context"
)
import consul "github.com/hashicorp/consul/api"
const (
	ServiceName = "payment"
	ServicePort = "80"
)

func main() {
	var (
		port          = flag.String("port", ServicePort, "Port to bind HTTP listener") // original port - "8080"
		zip           = flag.String("zipkin", os.Getenv("ZIPKIN"), "Zipkin address")
		declineAmount = flag.Float64("decline", 100005, "Decline payments over certain amount")
	)
	flag.Parse()
	var tracer stdopentracing.Tracer
	{
		// Log domain.
		var logger log.Logger
		{
			logger = log.NewLogfmtLogger(os.Stderr)
			logger = log.NewContext(logger).With("ts", log.DefaultTimestampUTC)
			logger = log.NewContext(logger).With("caller", log.DefaultCaller)
		}
		// Find service local IP.
		conn, err := net.Dial("udp", "8.8.8.8:80")
		if err != nil {
			logger.Log("err", err)
			os.Exit(1)
		}
		localAddr := conn.LocalAddr().(*net.UDPAddr)
		host := strings.Split(localAddr.String(), ":")[0]
		defer conn.Close()
		if *zip == "" {
			tracer = stdopentracing.NoopTracer{}
		} else {
			logger := log.NewContext(logger).With("tracer", "Zipkin")
			logger.Log("addr", zip)
			collector, err := zipkin.NewHTTPCollector(
				*zip,
				zipkin.HTTPLogger(logger),
			)
			if err != nil {
				logger.Log("err", err)
				os.Exit(1)
			}
			tracer, err = zipkin.NewTracer(
				zipkin.NewRecorder(collector, false, fmt.Sprintf("%v:%v", host, port), ServiceName),
			)
			if err != nil {
				logger.Log("err", err)
				os.Exit(1)
			}
		}
		stdopentracing.InitGlobalTracer(tracer)

	}
	// Mechanical stuff.
	errc := make(chan error)
	ctx := context.Background()

	handler, logger := payment.WireUp(ctx, float32(*declineAmount), tracer, ServiceName)

	// Create and launch the HTTP server.
	go func() {
		logger.Log("transport", "HTTP", "port", *port)
		errc <- http.ListenAndServe(":"+*port, handler)
	}()

	// Capture interrupts.
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// consul - begin -------------------------------------------
	c, err := consul.NewClient(&consul.Config{
		Address: "consul:8500",
	})
	if err != nil {
		logger.Log("Failed to connect to Consul agent: ", err)
	}
	
	ifaces, err := net.Interfaces()
	// handle err
	var ip net.IP
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
		}
	}

	var (
		ServiceIP = ip.String() // ip of container 
	)
	
	ServicePortInt, err := strconv.Atoi(ServicePort) // convert to int


	// To register multiple services with the same Name,
	// we must define directly the ID and the specific Address. 
	// To get a unique ID we use the associated IP as a suffix.
	serviceDef := &consul.AgentServiceRegistration{
		ID: fmt.Sprintf("%s-%s", ServiceName, ServiceIP),
		Name: ServiceName,
		Address: ServiceIP,
		Port: ServicePortInt,
		Check: &consul.AgentServiceCheck{
			Interval: "10s",
			HTTP: fmt.Sprintf("http://%s:%d/health", ServiceIP, ServicePortInt),
		},
	}
    
	//log.Printf("try to register the service")
	logger.Log("try to register the service")
	if err := c.Agent().ServiceRegister(serviceDef); err != nil {
		//log.Fatal("Failed to register Service: ", err)
		logger.Log("Failed to register Service: ", err)
	}
	// consul - end -----------------------------------------------
	

	logger.Log("exit", <-errc)
}
