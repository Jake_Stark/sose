import base64
import json

from locust import HttpLocust, TaskSet, task, between
from random import randint, choice


class WebTasks(TaskSet):

    @task
    def load(self):
        data = "user:password"
        base64Bytes  = base64.b64encode(data.encode("utf-8"))
        base64string = str(base64Bytes, "utf-8")
#print(base64string)
#       catalogue = self.client.get("/catalogue").json()
        response = self.client.get("/catalogue")
        catalogueJson = json.loads(response.content)
        category_item = choice(catalogueJson)
        item_id = category_item["id"]
        print(item_id)
        self.client.get("/")
        self.client.get("/login", headers={"Authorization":"Basic %s" % base64string})
        self.client.get("/category.html")
        self.client.get("/detail.html?id={}".format(item_id))
        self.client.delete("/cart")
        self.client.post("/cart", json={"id": item_id, "quantity": 1})
        self.client.get("/basket.html")
        self.client.post("/orders")


class Web(HttpLocust):
    task_set = WebTasks
    # deprecated min_wait = 0#max_wait = 0
    wait_time = between(0, 0)
