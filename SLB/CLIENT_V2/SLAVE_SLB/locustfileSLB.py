# SLB - 
# auto-discovery delle istance servizi di LBT1 

import base64
import json
import consul

from locust import HttpLocust, TaskSet, task, between
from random import randint, choice


class WebTasks(TaskSet):

    def on_start(self):
      # on_start: Viene eseguito solo all'avvio
      #
      # Effettuo auto-discovery ip per l'enpoint lbt1
      # recuperando ip e porta, per dinamicità e per rendere il 
      # file locust più simile agli altri di (HLB e CLB)
      # anche se in realtà conosciamo già l'endpoint (http://lbt1:80)
      #
      results = [] 
      service = consul.Client(endpoint='http://consul:8500')

      lbt1s = service.info(name='lbt1')
      for lbt in  lbt1s:
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))
        # N.B. per rendere la scelta simile agli altri locustfile, che scelgono tra 
        # 3 diversi elementi in un array appendiamo 2 elementi ai risultati
        # anche se l'enpoint scelto alla fine sarà sempre lo stesso 
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))
        results.append(lbt['ServiceAddress'] +":"+ str(lbt['ServicePort']))
      
      self.endpoints = results

      #per la login 
      data = "user:password"
      base64Bytes  = base64.b64encode(data.encode("utf-8"))
      self.base64string = str(base64Bytes, "utf-8")

    @task
    def load(self):

        # Per non invalidare i test, dobbiamo fare in modo che i locustfile siano
        # molto simili tra loro nelle tre varie configurazioni (HLB,CLB;SLB)
        # 
        # Importante: Ciclare in questo modo renderebbe i test diversi
        # oltre che ad ogni passo verrebbero fatte n-query una per ogni enpoints
        # for endpoint in self.endpoints:
        #   print("ENDPOINT:" + endpoint)
        
        # Invece ad ogni passo facciamo una scelta randomica tra i possibili endpoints
        endpoint = choice(self.endpoints)

        host = "http://" + endpoint 
        hostname = "lbt1_" + endpoint # ip:port

        response = self.client.get(host + "/catalogue", name=hostname+"/catalogue")
        catalogueJson = json.loads(response.content)
        category_item = choice(catalogueJson)
        item_id = category_item["id"]
        #print(item_id)
        self.client.get(host + "/", name=hostname+"/")
        self.client.get(host + "/login", headers={"Authorization":"Basic %s" % self.base64string}, name=hostname+"/login")
        self.client.get(host + "/category.html", name=hostname+"/category.html")
        self.client.get(host + "/detail.html?id={}".format(item_id), name=hostname+"/detail.html?id={}".format(item_id))
        self.client.delete(host + "/cart", name=hostname+"/cart")
        self.client.post(host + "/cart", json={"id": item_id, "quantity": 1}, name=hostname+"/cart")
        self.client.get(host + "/basket.html", name=hostname+"/basket.html")
        self.client.post(host + "/orders", name=hostname+"/orders")


class Web(HttpLocust):
    host ="http://lbt1:80" # viene sovrascritto (dallo stesso nel task load), necessario per locust
    task_set = WebTasks
    wait_time = between(0, 0) # wait 0
